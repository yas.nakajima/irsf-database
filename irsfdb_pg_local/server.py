#!/usr/bin/env python3

import sys
from http.server import HTTPServer, CGIHTTPRequestHandler

class Handler(CGIHTTPRequestHandler):
    cgi_directories = ["/cgi-bin"]

PORT = 8000

if len(sys.argv) > 1:
    inPORT = sys.argv[1]
    try:
        PORT = int(inPORT)
    except:
        print('usage : ./server.py [PORT](optional)')
        print('PORT must be integer')
        print('PORT is 8000 by default.')
        sys.exit()

httpd = HTTPServer(("", PORT), Handler)
httpd.serve_forever()

