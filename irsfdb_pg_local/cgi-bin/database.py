#!/usr/bin/env python3

print("Content-type: text/html")
print()
print("""
<html>
<body bgcolor="#FFFFFF">

<br>
<FONT size="5">
<b> IRSF Database (001127..200306)</b>
</FONT>
<br>
<pre> </pre>

<table border='0' cellspacing='0' cellpadding='10'>
<tr bgcolor="#F5F5F5">
<td>
<form method=post action='./results.py'>
<table>
<tr>
<td><b>Search by object name: </b> </td>
<td> &nbsp 'OBJECT' in the FITS header  </td>
</tr>
<tr>
<td>
</td>
<td>
<input type=hidden name='method' value='1'>
<input type=hidden name='status' value='1'>
<input type=radio name='status' value='1'>starting with / 
<input type=radio name='status' value='2' checked>including
<input type=text size=30 name='searchname' value=''> &nbsp case-insensitive
</td>
</tr>
<tr><td> show all dither positions?</td><td>
<input type=radio name='dither' value='1' checked> only the 1st frame from each dither set / 
<input type=radio name='dither' value='2'> all
</td>
</tr>
<tr><td> SIRIUS/SIRPOL </td><td>
<input type=radio name='sirpol' value='1' checked> SIRIUS+SIRPOL / 
<input type=radio name='sirpol' value='2'> only SIRIUS /
<input type=radio name='sirpol' value='3'> only SIRPOL
</td>
</tr>
<tr><td> Output option </td><td>
<input type=radio name='output' value='1' checked> HTML / 
<input type=radio name='output' value='2'> ASCII
</td>
</tr>
<tr><td></td><td align='center'>
<input type=submit value='    search    ' name='action'> 
</td>
</tr>
</form>
</table>
</td>
</tr>
<tr>
<td> <b> NOTE: </b> SIRIUS or SIRPOL is judged by 'pol_agl1' and 'pol_agl2' in the FITS header. 
If the frame has 'pol_agl1' and 'pol_agl2' in the FITS header, it is classified as SIRPOL. 
However, the FLAT frames don't have 'pol_agl1' and 'pol_agl2' in the FITS header, and they are classified as SIRIUS even if they are obtained with SIRPOL. The handwritten obslog and/or the classification of the other frames of the same night may help in classifying the FLAT frames. <br>
&nbsp 

</td>
</tr>
<tr bgcolor="#F5F5F5">
<td>
<form method=post action='./results.py'>
<table>
<tr>
<td> <b>Search by position: </b> </td>
<td> &nbsp 'RA' and 'DEC' of the FITS header within a radius of <input type=text size=4 name='rr' value=''> arcmin centered at  </td>
</tr>
<tr>
<td>
</td>
<td>
&nbsp RA(hh:mm:ss.s) <input type=text size=12 name='rastr' value=''> 
&nbsp Dec(dd:dm:ds.s) <input type=text size=12 name='decstr' value=''>
<input type=hidden name='method' value='2'>
</td>
</tr>
<tr>
<td> show all dither positions? </td>
<td>
<input type=radio name='dither' value='1' checked> only the 1st frame from each dither set / 
<input type=radio name='dither' value='2'> all
</td>
</tr>
<tr><td> SIRIUS/SIRPOL </td><td>
<input type=radio name='sirpol' value='1' checked> SIRIUS+SIRPOL /
<input type=radio name='sirpol' value='2'> only SIRIUS /
<input type=radio name='sirpol' value='3'> only SIRPOL
</td>
</tr>
<tr><td> Output option </td><td>
<input type=radio name='output' value='1' checked> HTML / 
<input type=radio name='output' value='2'> ASCII
</td>
</tr>
<tr>
<td> </td>
<td align='center'>
<input type=submit value='    search    ' name='action'> 
</td>
</tr>
</table>
</form>
</td>
</tr>
<tr>
<td> <b> NOTE : </b> 'RA' and 'DEC' are based on the telescope pointing, not WCS-calibrated. <br>
&nbsp 
</td>
</tr>
<tr bgcolor="#F5F5F5">
<td><b> Search by date </b><br>
The PDF file of handwritten obslog is linked at the page of each date on and after 050408. 
<table>
<tr>
<td> <form method=get><a href='./irsf_year.py?year=00'>2000</a></form> </td> 
<td> <form method=get><a href='./irsf_year.py?year=01'>2001</a></form> </td> 
<td> <form method=get><a href='./irsf_year.py?year=02'>2002</a></form> </td> 
<td> <form method=get><a href='./irsf_year.py?year=03'>2003</a></form> </td> 
<td> <form method=get><a href='./irsf_year.py?year=04'>2004</a></form> </td> 
<td> <form method=get><a href='./irsf_year.py?year=05'>2005</a></form> </td> 
<td> <form method=get><a href='./irsf_year.py?year=06'>2006</a></form> </td> 
<td> <form method=get><a href='./irsf_year.py?year=07'>2007</a></form> </td> 
<td> <form method=get><a href='./irsf_year.py?year=08'>2008</a></form> </td> 
<td> <form method=get><a href='./irsf_year.py?year=09'>2009</a></form> </td> 
</tr>
<tr>
<td> <form method=get><a href='./irsf_year.py?year=10'>2010</a></form> </td> 
<td> <form method=get><a href='./irsf_year.py?year=11'>2011</a></form> </td> 
<td> <form method=get><a href='./irsf_year.py?year=12'>2012</a></form> </td> 
<td> <form method=get><a href='./irsf_year.py?year=13'>2013</a></form> </td> 
<td> <form method=get><a href='./irsf_year.py?year=14'>2014</a></form> </td> 
<td> <form method=get><a href='./irsf_year.py?year=15'>2015</a></form> </td> 
<td> <form method=get><a href='./irsf_year.py?year=16'>2016</a></form> </td> 
<td> <form method=get><a href='./irsf_year.py?year=17'>2017</a></form> </td> 
<td> <form method=get><a href='./irsf_year.py?year=18'>2018</a></form> </td> 
<td> <form method=get><a href='./irsf_year.py?year=19'>2019</a></form> </td> 
</tr>
<tr>
<td> <form method=get><a href='./irsf_year.py?year=20'>2020</a></form> </td> 
</tr>

</table>
</td>
</tr>
<tr>
<td> <b> NOTE: </b> 'cflag' denotes the availability of each band, e.g, 'cflag = 111' means all the bands is available and '011' means J is not available.   
  </td>
</tr>
<tr bgcolor="#F5F5F5">
<td>
<form method=get><a href='./find_incomplete.py'>Show the dates including incomplete exposures.</a></form>
</td>
</tr>
</table>


</body>
</html>""")

