#!/usr/bin/env python3                                                                            

#
# showtable for postgresql 
# date_time is timestamp not string 
#

from astropy.coordinates import SkyCoord
from astropy import units as u
 
def deg2sexa(radeg, decdeg):

    try:
        skycoo = SkyCoord(radeg, decdeg, unit='deg')
        rasexa = skycoo.ra.to_string(unit=u.hourangle, sep=':', precision=1, pad=True)
        decsexa = skycoo.dec.to_string(sep=':', precision=1, pad=True)
    except:
        
        rasexa = 'None'
        decsexa = 'None'

    return rasexa, decsexa


def showHTML(rows):

    print(len(rows), 'hits')
    print("<table border='1' cellspacing='2' cellpadding='5'><caption><b> obslog </b></caption>")
    print("""
    <tr>
    <th> yymmdd </th>
    <th> fnum </th>
    <th> object </th>
    <th> itime </th>
    <th> date_time(UT) </th>
    <th> RA </th>
    <th> Dec </th>
    <th> airmass </th>
    <th> RA_OFF </th>
    <th> DEC_OFF </th>
    <th> pol-agl1 </th>
    <th> pol-agl2 </th>
    <th> cflag </th>
    </tr>""")

    for item in rows:

        if item[5] != None and item[6] != None:
            rasexa, decsexa = deg2sexa(item[5], item[6])
        else:
            rasexa = None
            decsexa = None

        try:
            airmass = '{:.3f}'.format(item[11])
        except:
            airmass = 'None'

        try:
            date_time = item[4].strftime("%Y-%m-%d %H:%M:%S")
        except:
            date_time = 'None'

        print("""
        <tr>
        <td> {0} </td>
        <td> {1} </td>
        <td> {2} </td>
        <td> {3} </td>
        <td> {4} </td>
        <td> {5} </td>
        <td> {6} </td>
        <td> {7} </td>
        <td> {8} </td>
        <td> {9} </td>
        <td> {10} </td>
        <td> {11} </td>
        <td> {12} </td>
        </tr>""".format(item[0], item[1], item[2], item[3], date_time, rasexa, 
                        decsexa, airmass, item[7], item[8], item[9], item[10], item[12]))
        
    print("""
    </table>""")



def showASCII(rows):

    print("<pre>")
    print("yymmdd fnum object itime date_time(UT) RA Dec airmass RA_OFF DEC_OFF pol-agl1 pol-agl2 cflag") 

    for item in rows:
        
        if item[5] != None and item[6] != None:
            rasexa, decsexa = deg2sexa(item[5], item[6])
        else:
            rasexa = None
            decsexa = None

        try:
            airmass = '{:.3f}'.format(item[11])
        except:
            airmass = 'None'

        try:
            date_time = item[4].strftime("%Y-%m-%d %H:%M:%S")
        except:
            date_time = 'None'

        print(item[0], item[1], item[2], item[3], date_time, rasexa, 
                        decsexa, airmass, item[7], item[8], item[9], item[10], item[12])

    print("</pre>")

