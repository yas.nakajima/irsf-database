#!/usr/bin/env python3

import math
import psycopg2
from astropy.coordinates import SkyCoord
from astropy import units as u

#
# global parameters for postgreSQL  
# 
database = 'irsf'
user = 'irsf'
password = 'set_postgresql_irsf_password_here'
#
#
#


def byobject(form):

    status = form.getvalue('status')
    sname = form.getvalue('searchname')
    dither = form.getvalue('dither')
    sirpol = form.getvalue('sirpol')

    # removing the BLANK at the beginning and the end  
    sname = sname.strip()

    if dither[-1] == '1':
        dtxt = 'raoff=0 and decoff=0 and '
    else:
        dtxt = ''

    if sirpol[-1] == '1':
        stxt = ''
    elif sirpol[-1] == '2':
        stxt = 'pol_agl1 IS NULL and '
    elif sirpol[-1] == '3':
        stxt = 'pol_agl1 IS NOT NULL and '

    if status[-1] == '1':
        qname = sname+'%'
    else:
        qname = '%'+sname+'%'

    # postgresql to use ilike for case-insensitive search 
    sql = "select * from obslog where " + dtxt + stxt + "object ilike '" + qname + "' ORDER BY yymmdd,fnum"

    conn = psycopg2.connect(database=database, user=user, password=password)
    cursor = conn.cursor()
    cursor.execute(sql)
    rows = cursor.fetchall()
    cursor.close()
    conn.close()

    return rows


def byradec(form):

    dither = form.getvalue('dither')
    sirpol = form.getvalue('sirpol')

    radiusm = form.getvalue('rr')
    rastr = form.getvalue('rastr')
    decstr = form.getvalue('decstr')

    skycoo = SkyCoord(rastr, decstr, unit=('hourangle', 'deg'))

    '''
    vra = rastr.split(':')
    hh = vra[0]
    mm = vra[1]
    ss = vra[2]
    vdec = decstr.split(':')
    dd = vdec[0]
    dm = vdec[1]
    ds = vdec[2]

    radeg_in = 15 * (int(hh) + int(mm) / 60.0 + float(ss) / 3600.0)
    decdeg_in = int(dd) + math.copysign(1, int(dd)) * (int(dm) / 60.0 + float(ds) / 3600.0)
    '''

    radeg_in = skycoo.ra.degree
    decdeg_in = skycoo.dec.degree

    rarad_in = math.radians(radeg_in)
    sinLat = math.sin(math.radians(decdeg_in))
    cosLat = math.cos(math.radians(decdeg_in))
    radiusd = float(radiusm)/60.0
    cosR = math.cos(math.radians(radiusd))

    constr1r = ""
    raradius = math.degrees(2.0 * math.asin( math.sin(math.radians(radiusd)*0.5) / cosLat ))

    ramin = radeg_in - raradius
    ramax = radeg_in + raradius

    if radeg_in + radiusd > 360:
        ramax = ramax - 360
        constr1r = "((radeg >= " + str(ramin) + ") OR (radeg <= " + str(ramax) + ")) AND "  
    elif radeg_in - radiusd < 0:
        ramin = ramin + 360
        constr1r = "((radeg >= " + str(ramin) + ") OR (radeg <= " + str(ramax) + ")) AND "  
    else:
        constr1r = "((radeg >= " + str(ramin) + ") AND (radeg <= " + str(ramax) + ")) AND "  
    
    constr1d = ""
    decmin = decdeg_in - radiusd
    decmax = decdeg_in + radiusd
    # if the target is close to one of the pole, no constraints on RA. 
    if decmin < -90:
        decmin = -90
        constr1r = ""
    elif decmax > 90:
        decmax = 90
        constr1r = ""
    
    constr1d = "((decdeg >= " + str(decmin) + ") AND (decdeg <= " + str(decmax) + "))"  

    constr1 = constr1r + constr1d 

    constr2 = "(" + str(cosR) + "<= (sin(decdeg*0.017453292519943295) * " + str(sinLat) \
             + " + cos(decdeg*0.017453292519943295) * " + str(cosLat) \
             + " * cos(radeg*0.017453292519943295 - " + str(rarad_in) + ")))" 

    if dither[-1] == '1':
        dtxt = 'raoff=0 AND decoff=0 AND '
    else:
        dtxt = ''

    if sirpol[-1] == '1':
        stxt = ''
    elif sirpol[-1] == '2':
        stxt = 'pol_agl1 IS NULL AND '
    elif sirpol[-1] == '3':
        stxt = 'pol_agl1 IS NOT NULL AND '

    sql = "SELECT * FROM obslog WHERE " + dtxt + stxt + constr1 + " AND " + constr2 \
          + " ORDER BY yymmdd,fnum"


    conn = psycopg2.connect(database=database, user=user, password=password)
    cursor = conn.cursor()
    cursor.execute(sql)
    rows = cursor.fetchall()
    cursor.close()
    conn.close()

    return rows



def byyear(year):

    conn = psycopg2.connect(database=database, user=user, password=password)
    cursor = conn.cursor()

    sql = "select distinct(yymmdd) from obslog where yymmdd like '{}' order by yymmdd".format(year+'%')   

    cursor.execute(sql)
    rows = cursor.fetchall()
    cursor.close()
    conn.close()

    return rows


def bydate(date):

    conn = psycopg2.connect(database=database, user=user, password=password)
    cursor = conn.cursor()

    sql = "select * from obslog where yymmdd like '{}' order by fnum".format(date)

    cursor.execute(sql)
    rows = cursor.fetchall()
    cursor.close()
    conn.close()

    return rows


def bycflag():

    ngdate = set([])

    conn = psycopg2.connect(database=database, user=user, password=password)
    cursor = conn.cursor()
    cursor.execute("select yymmdd from obslog where cflag not like '111' and object not like '' ")
    rows = cursor.fetchall()

    for item in rows:
        ngdate.add(item[0])

    cursor.close()
    conn.close()

    return ngdate



def bydate_cflag(date):

    conn = psycopg2.connect(database=database, user=user, password=password)
    cursor = conn.cursor()

    sql = "select * from obslog where yymmdd like '{}' and object not like '' and cflag not like '111' order by fnum".format(date)

    cursor.execute(sql)
    rows = cursor.fetchall()
    cursor.close()
    conn.close()

    return rows


