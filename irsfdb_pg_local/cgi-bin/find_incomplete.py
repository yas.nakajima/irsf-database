#!/usr/bin/env python3

import cgi
import cgitb
cgitb.enable()
import irsfsearch


print("Content-type: text/html")
print() 
print("""
<html>
<head>
<style type="text/css">
td {
}
td.top { vertical-align: top; }
td.middle { vertical-align: middle; }
td.bottom { vertical-align: bottom; }
</style>
</head>
<body bgcolor="#FFFFFF">
<b> yymmdd including incomplete exposures </b><br>
<table border='0' cellspacing='10' cellpadding='10'><tr><td class='top'> """)

ngdate = irsfsearch.bycflag()

prevyear = '00'
for item in sorted(ngdate):
    year = item[:2]
    if year != prevyear:
        print ("</td><td class='top'>")
    prevyear = year
    print("<form method=get><a href='./irsf_cflag_date.py?date="+item+"'>"+item+"</a></form>")

print("""
</td></tr></table>
</body>
</html>""")








