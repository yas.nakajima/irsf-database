#!/usr/bin/env python3

import cgi
import cgitb
cgitb.enable()
import irsfsearch

form = cgi.FieldStorage()
year = form.getvalue('year')

print("Content-type: text/html")
print() 
print("""
<html>
<head>
<style type="text/css">
td {
}
td.top { vertical-align: top; }
td.middle { vertical-align: middle; }
td.bottom { vertical-align: bottom; }
</style>
</head>
<body bgcolor="#FFFFFF">""")
# <b> 20%s </b><br> 
print("<b>" + year + "</b><br>")
print("<table border='0' cellspacing='10' cellpadding='10'><tr><td class='top'> ")

rows = irsfsearch.byyear(year)

prevmonth = '01'
for item in rows:
    date = item[0]
    month = date[2:4]
    if month != prevmonth:
        print("</td><td class='top'>")
    prevmonth = month
    print("<form method=get><a href='./irsf_date.py?date=%s'>%s</a></form>" % (date, date))

print("""
</td></tr></table>
</body>
</html>""")










