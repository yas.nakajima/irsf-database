#!/usr/bin/env python3

import os
import cgi
import cgitb
cgitb.enable()
import irsfsearch
import showtable

form = cgi.FieldStorage()
date = form.getvalue('date')

rows = irsfsearch.bydate_cflag(date)

print("Content-type: text/html")
print() 
print("""
<html>
<body bgcolor="#FFFFFF">
<head>""")

yy = date[:2]
if os.path.exists('./obslog_pdf/obslog_'+date+'.pdf'):
    print("<a href='../obslog_pdf/obslog_"+date+".pdf' download='obslog_"+date+".pdf'> click to open the obslog PDF </a><br><pre> </pre>")
else:
    print("<b>date</b><br><br>No obslog pdf.<br>")

showtable.showHTML(rows)

print("""
</body>
</html>""")



