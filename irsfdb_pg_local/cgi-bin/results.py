#!/usr/bin/env python3

import cgi
import irsfsearch
import sys
import showtable

print("Content-type: text/html")
print("""
<html>
<body bgcolor="#FFFFFF">""")

form = cgi.FieldStorage()
method = form.getvalue('method')
output = form.getvalue('output')

if method == '1':
    if not 'searchname' in form:
        print('Specify an object name')
        print('</body></html>')
        sys.exit()
    rows = irsfsearch.byobject(form)
        
else:
    if not 'rr' in form:
        print('Specify a width')
        print('</body></html>')
        sys.exit()
    elif not 'rastr' in form or not 'decstr' in form:
        print('Specify RA and DEC')
        print('</body></html>')
        sys.exit()

    rows = irsfsearch.byradec(form)

if output == '1':
    showtable.showHTML(rows)
elif output == '2':
    showtable.showASCII(rows)


print("""
</body>
</html>""")






