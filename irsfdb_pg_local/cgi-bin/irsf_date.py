#!/usr/bin/env python3

import os
import glob
import cgi
import cgitb
cgitb.enable()
import irsfsearch
import showtable

form = cgi.FieldStorage()
date = form.getvalue('date')
output = form.getvalue('output')

rows = irsfsearch.bydate(date)

print("Content-type: text/html")
print("""
<html>
<body bgcolor="#FFFFFF">
<head>""")

yy = date[:2]

if output == '2':
    showtable.showASCII(rows)
else:
    lists = glob.glob('./obslog_pdf/obslog_'+date+'*.pdf')
    if len(lists) > 0:
        for item in lists:
            path = '.'+item
            pdffile = item.split('/')[2]
            print("<a href='"+path+"' download='"+pdffile+"'> click to open "+pdffile  +"</a><br>")
    else:
        print("<b>date</b><br><br>No obslog pdf.<br>")
        
    print("""
    <pre> </pre>
    <form method=post action='./irsf_date.py'>
    <input type=hidden name='output' value='2'>""")
    print("<input type=hidden name='date' value='"+date+"'>") 
    print("""
    <input type=submit value='SHOW ASCII' name='action'>
    </form>""")

    showtable.showHTML(rows)

print("""
</body>
</html>""")

